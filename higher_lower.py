import os,random
from art import logo, vs
from game_data import data

def game():
    clear = lambda: os.system('cls' if os.name == 'nt' else 'clear')
    clear()
    print(f"{logo} \n")
    print("Instagram edition \n")
    print("Who has more Instagram Followers? \n")
    list_of_instagrammers = data.copy()
    score = 0
    game_over = False

    def more_followers(option_a,option_b):
        if option_a['follower_count'] > option_b['follower_count']:
            return option_a
        elif option_a['follower_count'] < option_b['follower_count']:
            return option_b

    def get_option():
        if not list_of_instagrammers:
            print("You have won max points. You are the master of this game.")
            return
        else:
            a =  random.choice(list_of_instagrammers)
            list_of_instagrammers.pop(list_of_instagrammers.index(a))
            return a
    #print(list_of_instagrammers)

    option_a = get_option()
    while not game_over:
        #print(list_of_instagrammers)
        option_b = get_option()
        if option_b:
            answer = more_followers(option_a,option_b)
            print(f"Compare A: {option_a['name']}, a {option_a['description']} from {option_a['country']}")
            print(vs)
            print(f"Against B: {option_b['name']}, a {option_b['description']} from {option_b['country']}")
            
            input_answer=input("\nWho has more followers? Type 'A' or 'B': ").upper()
            if option_a['follower_count'] > option_b['follower_count']:
                correct_answer = "A"
            elif option_a['follower_count'] < option_b['follower_count']:
                correct_answer = "B"
            if input_answer == correct_answer:
                score += 1
                option_a = answer
                print(f"\n{answer['name']} is the correct answer \n")
            else:
                game_over = True
                print("Sorry. Wrong Answer")
                print(f"Score: {score}")
                if input("Wanna play again? (Y/N)?: ").upper() == "Y":
                    game()
        else:
            game_over = True

game()
   
